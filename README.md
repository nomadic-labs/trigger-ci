# trigger-ci

## How to use this script ?

Step 1 - Create a personal access token from https://gitlab.com/-/profile/keys 'api' scope

Step 2 - Export the newly created token to your shell session :

```bash
export GITLAB_TOKEN=XXXXXXX
```

_Tip_ Declare this export on your ~/.bash_profile

Step 3 - Run the script

```bash
./trigger-ci.sh --project-id YOUR_GITLAB_PROJECT_ID --username YOUR_USERNAME
```

_Tip_ NL Tezos fork gitlab id is 9487506

Do not hesitate to contact NL Node & Tooling team for any questions or issue
