#!/usr/bin/env bash

# =========================
# How to use this script ?
# =========================
#
# Step 1 - Create a personal access token from https://gitlab.com/-/profile/keys 'api' scope
#
#
# Step 2 - Export the newly created token to your shell session :
#
#          $> export GITLAB_TOKEN=XXXXXXX
#
#          Tip : to avoid doing it every time, declare this export on you ~/.bash_profile
#
# Step 3 - Run the script :
#
#          $> ./trigger-ci.sh --project-id YOUR_GITLAB_PROJECT_ID --username YOUR_USERNAME
#
#          Tip : NL Tezos fork gitlab id is 9487506
#
#
# Do not hesitate to contact NL Node & Tooling team for any questions or issue
#

set -e

SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

function fatal(){
    message=$1
    >&2 echo -e "Error : ${message}"
    exit 1
}

function var_exist_or_fatal() {
    local in_error="false"
    for var in "$@"; do
        if [ -z ${!var+x} ]; then
            >&2 echo "Error : Variable ${var} is not set"
            in_error="true"
        fi
    done
    if [ "${in_error}" == "true" ]; then
        fatal "Please fix it and retry"
    fi
}

function usage(){
    fatal "Usage: $(basename $0) --project-id project_id --username username" 
}


#
# Main
#

[ "$#" -ne 4 ] && usage

AVAILABLE_OPTS=(
  "project-id"
  "username"
)

# read arguments
opts=$(getopt \
  --longoptions "$(printf "%s:," "${AVAILABLE_OPTS[@]}")" \
  --name "$(basename "$0")" \
  --options "" \
  -- "$@"
)

eval set --$opts

while [[ $# -gt 0 ]]; do
  case "$1" in
    --project-id)
      project_id=$2
      shift 2
      ;;
    --username)
      username=$2
      shift 2
      ;;
    --help)
      usage
      exit
      ;;
    --)
      break
      ;;
    *)
      usage
      ;;
  esac
done

if ! command -v curl &> /dev/null; then
  fatal "curl could not be found, please install it before using this script."
fi

[ -z "$GITLAB_TOKEN" ] && fatal "GITLAB_TOKEN is not set, please create a personal access token from https://gitlab.com/-/profile/keys with api scope and export it in your shell session"

var_exist_or_fatal project_id username

api_base_url="https://gitlab.com/api/v4/projects/${project_id}"

echo -e "Waiting 45 seconds ..."
sleep 45

last_pipeline_id=$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "${api_base_url}/pipelines?per_page=1&page=1&username=${username}" | jq .[0].id)
([ -z "$last_pipeline_id" ] || [ "$last_pipeline_id" == "null" ]) && fatal "No pipeline found for user ${username}"

manual_job_id=$(curl -s "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "${api_base_url}/pipelines/${last_pipeline_id}/jobs?scope=manual" | jq -r '.[] | select(.name=="trigger") | .id')
([ -z "$manual_job_id" ] || [ "$manual_job_id" == "null" ]) && fatal "No available trigger found for username ${username}"

echo "Starting pipeline ${manual_job_id} from trigger job ${manual_job_id}"
web_url=$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" --request POST "${api_base_url}/jobs/${manual_job_id}/play" | jq -r '.pipeline.web_url')
echo -e "\n🍰 Success 🍰 \n\n➡️ pipeline started and could be followed-up on ${web_url}\n\n"

